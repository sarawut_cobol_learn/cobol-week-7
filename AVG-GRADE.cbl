       IDENTIFICATION DIVISION. 
       PROGRAM-ID. AVG-GRADE.
       AUTHOR. SARAWUT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
           ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg.txt"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD GRADE-FILE.
       01 GRADE-DETAILS.
          88 END-OF-GRADE-FILE                VALUE HIGH-VALUE.
       05 SUBJECT-DETAIL.
          10 SUBJECT-ID         PIC 9(6).
          10 SUBJECT-NAME       PIC X(50).
          10 SUBJECT-CREDIT     PIC 9.
          10 SUBJECT-GRADE      PIC X(2).
       FD AVG-GRADE-FILE.
       01 GRADE-OVERALL.
          10 GPAX-GRADE         PIC 9V9(3).
          10 GPAX-SCI-GRADE     PIC 9V9(3).
          10 GPAX-CS-GRADE      PIC 9V9(3).
       

       WORKING-STORAGE SECTION. 
       01 STU-DETAIL.          
          10 STU-GPAX           PIC 9V9(3).   
          10 STU-SCI-GPAX       PIC 9V9(3). 
          10 STU-CS-GPAX        PIC 9V9(3). 
       01 TEMP.
          10 SUM-AVG-GRADE      PIC 9(3)V9(3) VALUE ZEROS.
          10 SUM-CREDIT         PIC 9(3)      VALUE ZEROS.     
          10 SUM-AVG-SCI-GRADE  PIC 9(3)V9(3) VALUE ZEROS.
          10 SUM-SCI-CREDIT     PIC 9(3)      VALUE ZEROS.  
          10 SUM-AVG-CS-GRADE   PIC 9(3)V9(3) VALUE ZEROS.
          10 SUM-CS-CREDIT      PIC 9(3)      VALUE ZEROS.  
          10 GRADE-TEMP         PIC 9V9.  
          10 COUNTING-GPAX      PIC 99        VALUE ZEROS.
          10 COUNTING-SCI-GPAX  PIC 99        VALUE ZEROS.
          10 COUNTING-CS-GPAX   PIC 99        VALUE ZEROS.
       01 FILTER                PIC 9(2).
          88 ALL-SUBJECT                      VALUE 00.
          88 SCI-SUBJECT                      VALUE 30 THROUGH 39.
          88 CS-SUBJECT                       VALUE 31.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 000-FILES THRU 000-EXIT           
           PERFORM 002-WRITE-FILE THRU 002-EXIT
           GOBACK.

       000-FILES.
           OPEN INPUT GRADE-FILE
           PERFORM UNTIL END-OF-GRADE-FILE 
                   READ GRADE-FILE 
                   AT END                   
                      SET END-OF-GRADE-FILE TO TRUE
                      PERFORM 002-GETTER-GPAX THRU 002-EXIT
                   END-READ
                   IF NOT END-OF-GRADE-FILE THEN
                      PERFORM 001-GRADE-CONVERT THRU 001-EXIT
                      PERFORM 001-GPAX-COLLECT THRU 001-EXIT
                   END-IF 
           END-PERFORM           
           CLOSE GRADE-FILE
           .

       000-EXIT.
           EXIT.

       001-GPAX-COLLECT.
           MOVE SUBJECT-ID(1:2) TO FILTER 

           IF SCI-SUBJECT
              COMPUTE COUNTING-SCI-GPAX = COUNTING-SCI-GPAX + 1
              COMPUTE SUM-AVG-SCI-GRADE = SUM-AVG-SCI-GRADE +
                 (GRADE-TEMP * SUBJECT-CREDIT)
              COMPUTE SUM-SCI-CREDIT = SUM-SCI-CREDIT + SUBJECT-CREDIT 
           END-IF 

           IF CS-SUBJECT
              COMPUTE COUNTING-CS-GPAX = COUNTING-CS-GPAX + 1
              COMPUTE SUM-AVG-CS-GRADE = SUM-AVG-CS-GRADE +
                 (GRADE-TEMP * SUBJECT-CREDIT)
              COMPUTE SUM-CS-CREDIT = SUM-CS-CREDIT + SUBJECT-CREDIT  
           END-IF 
                               
           COMPUTE COUNTING-GPAX = COUNTING-GPAX + 1
           COMPUTE SUM-AVG-GRADE = SUM-AVG-GRADE +(GRADE-TEMP *
              SUBJECT-CREDIT)
           COMPUTE SUM-CREDIT = SUM-CREDIT + SUBJECT-CREDIT 
           .

       001-GRADE-CONVERT.
           EVALUATE TRUE 
           WHEN SUBJECT-GRADE = "A"
                MOVE "4" TO GRADE-TEMP
           WHEN SUBJECT-GRADE = "B+"
                MOVE "3.5" TO GRADE-TEMP
           WHEN SUBJECT-GRADE = "B"
                MOVE "3" TO GRADE-TEMP
           WHEN SUBJECT-GRADE = "C+"
                MOVE "2.5" TO GRADE-TEMP
           WHEN SUBJECT-GRADE = "C"
                MOVE "2" TO GRADE-TEMP
           WHEN SUBJECT-GRADE = "D+"
                MOVE "1.5" TO GRADE-TEMP
           WHEN SUBJECT-GRADE = "D"
                MOVE "1" TO GRADE-TEMP
           WHEN SUBJECT-GRADE = "F"
                MOVE "0" TO GRADE-TEMP
           END-EVALUATE 
           .

       001-EXIT.
           EXIT.

       002-GETTER-GPAX.
           COMPUTE STU-GPAX = SUM-AVG-GRADE / SUM-CREDIT
           COMPUTE STU-SCI-GPAX = SUM-AVG-SCI-GRADE / SUM-SCI-CREDIT
           COMPUTE STU-CS-GPAX = SUM-AVG-CS-GRADE / SUM-CS-CREDIT 

           DISPLAY "AVG-GRADE: " STU-GPAX
           MOVE STU-GPAX TO GPAX-GRADE

           DISPLAY "AVG-SCI-GRADE: " STU-SCI-GPAX
           MOVE STU-SCI-GPAX TO GPAX-SCI-GRADE 

           DISPLAY "AVG-CS-GRADE: " STU-CS-GPAX
           MOVE STU-CS-GPAX TO GPAX-CS-GRADE            
           .

       002-WRITE-FILE.
           OPEN OUTPUT AVG-GRADE-FILE 
           WRITE GRADE-OVERALL
           CLOSE AVG-GRADE-FILE
           .

       002-EXIT.
           EXIT.
